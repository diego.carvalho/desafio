from django.shortcuts import render, redirect, get_object_or_404
from .forms import *

def contato_new(request, template_name='contato_form.html'):
    form = ContatosForm(request.POST or None, request.Files or None)

    dados = {'form': form}

    if form.is_valid():
        form.save()
        return redirect('contatos_list')
    return render(request, template_name, dados)

def contatos_list(request, template_name ='contatos.html'):
    Contatos = Contato.objects.all()
    dados = {'contatos':Contatos}

    return render(request, template_name, dados)

def contato_update(request,id,template_name='contato_form.html'):
    contato = get_object_or_404(Contato, pk=id)
    form = ContatosForm(request.POST or None, request.Files or None, instace = contato)

    dados = {'form': form}
    if form.is_valid():
        form.save()
        return redirect('contatos_list')
    return render(request, template_name, dados)

def contato_delete(request,id,template_name='contato_delete.html'):
    client = get_object_or_404(Contato,pk=id)

    if request.method =='POST':
        client.delete()
        return redirect('contatos_list')

    return render(request, template_name)
