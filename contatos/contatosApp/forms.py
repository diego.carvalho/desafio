from django.forms import ModelForm
from .models import Contato

class ContatosForm(ModelForm):
    class Meta:
        model = Contato
        fields = ['first_name','last_name','phone','email','date_nasciment','cpf']
