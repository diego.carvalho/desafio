from django.urls import path
from .views import *

urlpatterns = {
    path('lista/',contatos_list, name ='contatos_list'),
    path('new/',contato_new,name='contato_new'),
    path ('update/<int:id>/', contato_update, name = 'contato_update'),
    path ('delete/<int:id>/', contato_delete, name = 'contato_delete')
}