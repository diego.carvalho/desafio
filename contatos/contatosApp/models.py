from django.db import models

# Create your models here.
class Contato (models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    phone = models.CharField(max_length=12)
    email = models.CharField(max_length=60)
    date_nasciment = models.DateField()
    cpf = models.CharField(max_length=14)

    def __str__(self):
        return self.first_name + ' '+ self.last_name